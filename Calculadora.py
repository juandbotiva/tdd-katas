__author__ = 'gloria'

import re

class Calculadora:
    def sumar(self,cadena):
        if cadena == '':
            return 0
        else:
            suma = 0
            for numero in re.split('\,|\:|\&', cadena):
                suma += int(numero)
            return suma
