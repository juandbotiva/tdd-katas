__author__ = 'gloria'

import unittest
from Calculadora import Calculadora


class CalculadoraTestCase(unittest.TestCase):
    def test_sumar_vacia(self):

        self.assertEqual(Calculadora().sumar(""),0,"Cadena vacia")

    def test_sumar_cadenaConUnNumero(self):
        self.assertEqual(Calculadora().sumar("1"),1,"Un numero")
        self.assertEqual(Calculadora().sumar("2"),2,"Un numero")

    def test_sumar_cadenaConDosNumeros(self):
        self.assertEqual(Calculadora().sumar("1,2"),3,"Dos numeros")
        self.assertEqual(Calculadora().sumar("5,4"),9,"Dos numeros")

    def test_sumar_cadenaConMasDeDosNumeros(self):
        self.assertEqual(Calculadora().sumar("0,1,2"), 3, "Mas de dos numeros")
        self.assertEqual(Calculadora().sumar("3,4,5,6"), 18, "Mas de dos numeros")
        self.assertEqual(Calculadora().sumar("11,10,9,8,7"), 45, "Mas de dos numeros")

    def test_sumar_cadenaOtrosSeparadores(self):
        self.assertEqual(Calculadora().sumar("1:2"),3,"Otros separadores")
        self.assertEqual(Calculadora().sumar("1&2"),3,"Otros separadores")
        self.assertEqual(Calculadora().sumar("0:1,2"), 3, "Otros separadores")
        self.assertEqual(Calculadora().sumar("3,4&5:6"), 18, "Otros separadores")
        self.assertEqual(Calculadora().sumar("11&10:9,8:7"), 45, "Otros separadores")

if __name__ == '__main__':
    unittest.main()
